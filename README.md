# API POC REDIS

_Prerequisitos:_
* docker version 1.10.3
* JDK8
* Maven

## Instalação

1. Execute esse comando no raiz do projeto:

   `mvn clean package && java -jar target/builders_client.jar`
   
2. Uma levantado o spring segue a url do Swagger:

   Entrypoint Swagger >> `http://localhost:8080/swagger-ui.html#`  
   
3. Na raiz do projeto tem uma collection do postman com os endpoints, e no start do projeto já é dado uma carga no H2 com alguns clientes.


## Stack

- `Redis`
- `Springboot` 
- `Swagger` 
- `Liquibase` 
- `H2` 
- `Lombock` 
- `Querydsl` 



