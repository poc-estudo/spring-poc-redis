package br.client.builders.api;

import br.client.builders.model.filter.ClienteFilter;
import br.client.builders.model.response.Pagination;
import br.client.builders.model.response.ResponseApi;
import br.client.builders.model.response.ResponseBuilder;
import br.client.builders.model.dto.ClienteDTO;
import br.client.builders.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/clientes")
public class ClienteController {


	@Autowired
	private ClienteService service;

	@Autowired
	private ResponseBuilder responseBuilder;

	@CrossOrigin(origins = "*")
	@GetMapping
	public ResponseEntity<ResponseApi<ClienteDTO>> consulta(@Valid ClienteFilter filter, @Valid Pagination page) {
		return responseBuilder.ok().content(service.listBy(filter, page)).build(ClienteDTO.class);
	}

	@PostMapping
	public ResponseEntity<ClienteDTO> create(@Valid @RequestBody ClienteDTO dto) {
		return new ResponseEntity<>(service.create(dto), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<ClienteDTO> update(@Valid @RequestBody ClienteDTO dto) {
		return new ResponseEntity<>(service.update(dto), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ClienteDTO> delete(@PathVariable("id") Long id) {
		service.delete(id);
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}



}
