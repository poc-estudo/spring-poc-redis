package br.client.builders.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.io.Closeable;
import java.time.Duration;
import java.util.List;


@Slf4j
@Configuration
public class RedisConfig extends CachingConfigurerSupport{


    @Value("${redis.server.ttl}")
    private long ttl;


    /**
     * Bean para tratamento de erros ao acessar o cache
     */
    @Override
    @Bean
    public CacheErrorHandler errorHandler() { return new AppCacheErrorHandler();}

    @Bean
    public RedisCacheConfiguration redisCacheConfiguration() {

        return RedisCacheConfiguration.defaultCacheConfig()
                .disableCachingNullValues()
                .entryTtl(Duration.ofSeconds(ttl))
                .disableKeyPrefix()
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()));
    }


    /**
     * Configure custom redisTemplate
     * @return
     */
    @Bean
    RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {

        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        // The serialization of the set value (value) uses Jackson2JsonRedisSerializer.
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        // Set the key (key) serialization using StringRedisSerializer.
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());

        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }


    /**
     * Inner class
     * Implementação para ignorar erros que possam ocorrer ao acessar o Redis
     */
    public class AppCacheErrorHandler implements CacheErrorHandler {


        @Override
        public void handleCacheGetError(RuntimeException e, Cache cache, Object o) {
            log.error("Error na operação GET do Redis", e);
        }

        @Override
        public void handleCachePutError(RuntimeException e, Cache cache, Object o, Object o1) {
            log.error("Error na operação PUT do Redis", e);
        }

        @Override
        public void handleCacheEvictError(RuntimeException e, Cache cache, Object o) {
            log.error("Error na operação EVICT do Redis", e);
        }

        @Override
        public void handleCacheClearError(RuntimeException e, Cache cache) {
            log.error("Error na operação CLEAR do Redis", e);
        }
    }


    /**
     * Inner clas
     * Extensão para retornar nulo caso ocorra algum erro ao executar comandos no Redis,
     * desta forma a aplicação continua funcionando caso tenha algum problema no cache.
     */
//    public class AppRedisTemplate<K, V> extends RedisTemplate<K, V> {
//
//        @Override
//        public <T> T execute(RedisCallback<T> action) {
//            try{
//                return super.execute(action);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public <T> T execute(RedisCallback<T> action, boolean exposeConnection) {
//            try{
//                return super.execute(action, exposeConnection);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public <T> T execute(RedisCallback<T> action, boolean exposeConnection, boolean pipeline) {
//            try{
//                return super.execute(action, exposeConnection, pipeline);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public <T> T execute(SessionCallback<T> session) {
//            try{
//                return super.execute(session);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public List<Object> executePipelined(SessionCallback<?> session) {
//            try{
//                return super.executePipelined(session);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public List<Object> executePipelined(SessionCallback<?> session, RedisSerializer<?> resulRedisSerializer) {
//            try{
//                return super.executePipelined(session, resulRedisSerializer);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public <T> T execute(RedisScript<T> script, List<K> keys, Object... args) {
//            try{
//                return super.execute(script, keys, args);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public <T> T execute(RedisScript<T> script, RedisSerializer<?> argsSerializer, RedisSerializer<T> resultSerializer, List<K> keys, Object... args) {
//            try{
//                return super.execute(script, argsSerializer, resultSerializer, keys, args);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        public <T extends Closeable> T executeWithStickyConnection(RedisCallback<T> callback) {
//            try{
//                return super.executeWithStickyConnection(callback);
//            }catch (Exception e) {
//                return null;
//            }
//        }
//    }







}