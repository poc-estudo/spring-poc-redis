package br.client.builders.exception;

import java.net.ConnectException;

public class RedisConnectionException extends ConnectException {
    public RedisConnectionException(String message) { super(message);}
}
