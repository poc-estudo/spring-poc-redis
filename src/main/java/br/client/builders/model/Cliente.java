package br.client.builders.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
@Entity
@SequenceGenerator(name = "seq_generator", sequenceName = "SEQ_CLIENTE", initialValue = 5, allocationSize = 1)
public class Cliente {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String cpf;
    private LocalDate nascimento;

    public long getIdade() {
        return ChronoUnit.YEARS.between(getNascimento(), LocalDate.now());
    }


}
