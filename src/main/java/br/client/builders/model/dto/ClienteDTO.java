package br.client.builders.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class ClienteDTO implements Serializable {

    private Long id;
    private String nome;
    private String cpf;
    private LocalDate nascimento;
    private long idade;

}
