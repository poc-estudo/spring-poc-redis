package br.client.builders.model.dto;

public interface DtoMapper <D, E> {

    D toDto(E entity);
    
}
