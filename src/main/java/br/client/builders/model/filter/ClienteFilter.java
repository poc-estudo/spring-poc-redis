package br.client.builders.model.filter;

import br.client.builders.model.QCliente;
import br.client.builders.model.response.Pagination;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.Getter;
import lombok.Setter;

import com.querydsl.core.types.Predicate;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.querydsl.core.BooleanBuilder;

import java.time.LocalDate;


@Getter
@Setter
public class ClienteFilter implements PredicateQueryFilter {

    private Long id;

    private String nome;

    private String cpf;

    private LocalDate nascimento;


    @Override
    public Predicate build() {
        BooleanBuilder builder = new BooleanBuilder();

        if(id != null) {
            builder.and(QCliente.cliente.id.eq(getId()));
        }

        if (StringUtils.isNotBlank(getNome())) {
            builder.and(QCliente.cliente.nome.containsIgnoreCase(getNome()));
        }

        if (StringUtils.isNotBlank(getCpf())) {
            builder.and(QCliente.cliente.cpf.eq(getCpf()));
        }

        return builder;
    }

    public Pageable getPageRequest(Pagination pagination) {
        return PageRequest.of(pagination.getPage(), pagination.getTotalElements(), Sort.Direction.ASC, "nome");
    }

}
