package br.client.builders.model.filter;

import com.querydsl.core.types.Predicate;

public interface PredicateQueryFilter {

    Predicate build();
}
