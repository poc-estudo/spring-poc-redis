package br.client.builders.model.mapper;

import br.client.builders.model.dto.DtoMapper;
import br.client.builders.model.dto.ClienteDTO;
import br.client.builders.model.Cliente;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;


@Mapper(componentModel = "spring")
public abstract class ClienteMapper implements DtoMapper<ClienteDTO, Cliente> {

    @Override
    public abstract ClienteDTO toDto(Cliente cliente);

    @AfterMapping
    public void convert(Cliente cliente, @MappingTarget ClienteDTO dto) {
        dto.setIdade(cliente.getIdade());
    }


}

