package br.client.builders.model.response;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Elements<D> implements Serializable {

	private final Integer page;

	private final Integer totalPages;

	private final Integer totalElements;

	private List<D> elements;

	public Elements(Page<?> page) {
		this.page = page.getNumber() + 1;
		this.totalElements = Long.valueOf(page.getTotalElements()).intValue();
		this.totalPages = page.getTotalPages();
	}
	
	public void setElements(List<D> elements) {
		this.elements = elements;
	}

}
