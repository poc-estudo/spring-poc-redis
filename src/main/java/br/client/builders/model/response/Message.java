package br.client.builders.model.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Message implements Serializable {

	private static final long serialVersionUID = 1L;

	private String type;

	private String key;
	
	private String value;

	private Serializable[] parameters;

	public Message(String tipo, String value) {
		this.type = tipo;
		this.value = value;
	}

	public Message(String key) {
		this.key = key;
	}

	public Message(String valor, String key, Serializable... parameters) {
		this.value = valor;
		this.key = key;
		this.parameters = parameters;
	}

}
