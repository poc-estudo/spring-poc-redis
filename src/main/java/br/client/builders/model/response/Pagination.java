package br.client.builders.model.response;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Pagination {
	
	private static final Integer DEFAULT_TOTAL_ELEMENTS = 30;

	@Min(1)
	private Integer page;

	@Min(1)
	@Max(300)
	private Integer totalElements;
	
	private Integer totalPages;

	public Integer getTotalElements() {
		if (totalElements == null) {
			return DEFAULT_TOTAL_ELEMENTS;
		}
		return totalElements;
	}

	public Integer getPage() {
		if (page == null) {
			return 0;
		}
		return page - 1;
	}
	

}
