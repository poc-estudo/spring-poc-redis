package br.client.builders.model.response;

import br.client.builders.model.dto.DtoMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;


@Component
public class PaginationFactory {

	public <E, D> Elements<D> toDto(Page<E> lista, DtoMapper<D, E> mapper) {

		Elements<D> paginacao = new Elements<>(lista);
		paginacao.setElements(lista.map(mapper::toDto).getContent());

		return paginacao;
	}
	
}