package br.client.builders.model.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseApi<T> {

	private List<Message> messages = new ArrayList<>();

	private Pagination pagination;

	private T content;

	public ResponseApi<T> add(Message message) {
		this.messages.add(message);
		return this;
	}

}
