package br.client.builders.model.response;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ResponseBuilder {

	private ResponseApi<Object> response = new ResponseApi<Object>();

	private Integer statusCode;

	private Class<?> classType;

	public ResponseBuilder content(Elements<?> elements) {
		this.response.setContent(elements.getElements());
		this.response.setPagination(new Pagination(elements.getPage(), elements.getTotalElements(), elements.getTotalPages()));
		return this;
	}
	
	public ResponseBuilder content(Object object) {
		this.response.setContent(object);
		return this;
	}
	
	public ResponseBuilder ok() {
		this.statusCode = HttpStatus.OK.value();
		return this;
	}

	public ResponseBuilder badRequest() {
		this.statusCode = HttpStatus.BAD_REQUEST.value();
		return this;
	}

	public ResponseBuilder add(List<Message> messages) {
		messages.forEach(this::add);
		return this;
	}

	public ResponseBuilder add(Message message) {
		this.response.add(message);
		return this;
	}

	public ResponseBuilder status(HttpStatus status) {
		this.statusCode = status.value();
		return this;
	}

	@SuppressWarnings("rawtypes")
	public ResponseApi build() {
		return response;
	}

	@SuppressWarnings("unchecked")
	public <T> ResponseEntity<ResponseApi<T>> build(Class<T> classType) {
		this.classType = classType;
		return ResponseEntity.status(statusCode).body((ResponseApi<T>) response);
	}

	public Class<?> getClassType() {
		return classType;
	}

}
