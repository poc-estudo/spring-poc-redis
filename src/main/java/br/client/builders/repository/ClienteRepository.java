package br.client.builders.repository;

import br.client.builders.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

@Repository
public interface ClienteRepository extends   JpaRepository<Cliente, Long> , QuerydslPredicateExecutor<Cliente>  {
}
