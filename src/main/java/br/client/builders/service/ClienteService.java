package br.client.builders.service;

import br.client.builders.model.filter.ClienteFilter;
import br.client.builders.model.response.Elements;
import br.client.builders.model.response.Pagination;
import br.client.builders.model.dto.ClienteDTO;

public interface ClienteService {

	Elements<ClienteDTO> listBy(ClienteFilter filter, Pagination page);

    ClienteDTO create(ClienteDTO dto);

    ClienteDTO update(ClienteDTO dto);

    void delete(Long id);
}
