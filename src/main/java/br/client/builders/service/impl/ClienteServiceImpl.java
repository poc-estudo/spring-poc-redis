package br.client.builders.service.impl;

import br.client.builders.model.filter.ClienteFilter;
import br.client.builders.model.response.Elements;
import br.client.builders.model.response.Pagination;
import br.client.builders.model.response.PaginationFactory;
import br.client.builders.model.dto.ClienteDTO;
import br.client.builders.model.mapper.ClienteMapper;
import br.client.builders.model.Cliente;
import br.client.builders.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.client.builders.service.ClienteService;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;


@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository repository;

	@Autowired
	private ClienteService service;

	@Autowired
	private PaginationFactory paginationFactory;

	@Autowired
	private ClienteMapper mapper;


	@Override
	@Cacheable(
			value = "cliente",
			key = "#page.page"
	)
	public Elements<ClienteDTO> listBy(ClienteFilter filter, Pagination page) {

		Page<Cliente> clientList = repository.findAll(filter.build(), filter.getPageRequest(page));

		return paginationFactory.toDto(clientList, mapper);
	}

	@PostMapping
	public ClienteDTO create(ClienteDTO dto) {

		Cliente cliente = Cliente.builder()
				           .id(null)
				           .cpf(dto.getCpf())
				           .nome(dto.getNome())
				           .nascimento(dto.getNascimento()).build();

		repository.saveAndFlush(cliente);

		return  mapper.toDto(cliente);
	}

	@Override
	public ClienteDTO update(ClienteDTO dto) {
		Cliente cliente = Cliente.builder()
				.id(dto.getId())
				.cpf(dto.getCpf())
				.nome(dto.getNome())
				.nascimento(dto.getNascimento())
				.build();

		repository.saveAndFlush(cliente);
		return  mapper.toDto(cliente);
	}

	@Override
	public void delete(Long id) {
		Optional<Cliente> cliente = repository.findById(id);
		repository.delete(cliente.get());
	}


}
